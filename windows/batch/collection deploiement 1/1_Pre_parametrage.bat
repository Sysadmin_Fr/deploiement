@echo off
set EXITCODE=0

set uriproxy="http://confproxy.proxy.domaine.fr/proxy.pac"
set kmsdom=principal.kms.domaine.fr:1688
set kmskey=xxxxx-xxxxx-xxxxx-xxxxx-xxxxx

:: Police Suppl�mentaire (T�l�charger les fichiers � partir des liens ci-dessous et renomm� en fonts_xYY.cab et copier les fichiers dans le dossier Pack\xYY)
:: x64 = http://download.windowsupdate.com/c/msdownload/update/software/updt/2016/09/microsoft-windows-languagefeatures-fonts-paneuropeansupplementalfonts-package_860a1e7e7aa6d3ff2c8667726616836b7502fa19.cab
:: x86 = http://download.windowsupdate.com/d/msdownload/update/software/updt/2016/10/microsoft-windows-languagefeatures-fonts-paneuropeansupplementalfonts-package_bb331a13ae0b2318866fb6466992292275f645f5.cab
:: Net framework 3.5 
:: Copier le fichier 'microsoft-windows-netfx3-ondemand-package.cab' du dossier /sources/sxs de votre ISO dans le dossier Pack\xYY

:: D�termine l'architecture du PC hote 
IF EXIST "%WinDir%\SysWOW64" SET HOSTArch=x64
IF NOT EXIST "%WinDir%\SysWOW64" SET HOSTArch=x86

PUSHD "%CD%"
CD /D "%~dp0"

:: Installation de Net Framework 3.5
 DISM /online /Enable-Feature /FeatureName:Netfx3 /All /Source:%~dp0Pack\%HOSTArch%\ /LimitAccess

:: Import des associations
dism /online /import-defaultAppAssociations:%~dp0Pack\w10x_DefaultAppsAssoc.xml

:: Ajout des nouvelles polices Microsoft
dism /online /Add-Package /Packagepath:%~dp0Pack\%HOSTArch%\fonts_%HOSTArch%.cab

:: Installation des Runtimes VC++
START /WAIT %~dp0Pack\VBCRedist_AIO_x86_x64.exe /y

:: Modification du menu
Powershell Import-StartLayout -LayoutPath %~dp0Pack\LayoutModification.xml -MountPath $env:SystemDrive\

:: Param�tres / Personnalisation / Accueil / D�sactivez: Afficher occasionnellement les suggestions dans l'�cran d'accueil
REG ADD "HKCU\Software\Microsoft\Windows\CurrentVersion\ContentDeliveryManager" /v "SystemPaneSuggestionsEnabled" /t REG_DWORD /d 0 /f 

:: D�sactiver le t�l�chargement et l'installation automatique des mises � jour des applications Metro
REG ADD "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\WindowsStore\WindowsUpdate" /v "AutoDownload" /t REG_DWORD /d "2" /f
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\WindowsStore" /v "AutoDownload" /t REG_DWORD /d "2" /f

:: D�sactiver le t�l�chargement et l'installation automatique des mises � jour des applications sugg�r�es
REG ADD "HKCU\Software\Microsoft\Windows\CurrentVersion\ContentDeliveryManager" /v "SilentInstalledAppsEnabled" /t REG_DWORD /d "0" /f

:: Configuration du Proxy
 REG ADD "HKCU\Software\Microsoft\Windows\CurrentVersion\Internet Settings" /v "ProxyEnable" /t REG_DWORD /d 0 /f
 REG ADD "HKCU\Software\Microsoft\Windows\CurrentVersion\Internet Settings" /v "AutoConfigUrl" /t REG_SZ /d %uriproxy% /f

:: Cr�ation d'un active setup pour d�sactiver la t�l�m�trie qui se retrouve d�sactiv� apr�s installation
::MKDIR %Public%\ConfTelemetrie
::copy %~dp0Pack\reg_activesetup.bat %Public%\ConfTelemetrie\reg_activesetup.bat /y

:: raccourcis
copy "%~dp0Pack\Raccourcis\Internet Explorer.lnk"         "%ProgramData%\Microsoft\Windows\Start Menu"
copy "%~dp0Pack\Raccourcis\Verrouiler le PC.lnk"          "%USERPROFILE%\Desktop"
copy "%~dp0Pack\Raccourcis\Vider le presse papier.lnk"    "%USERPROFILE%\Desktop"

:: Activation avec serveur KMS
slmgr.vbs /skms %kmsdom%
slmgr.vbs /ipk %kmskey% 
timeout /t 5
slmgr.vbs /ato

xcopy "%~dp0Pack\ClearIECache.cmd"  "%windir%\System32\GroupPolicy\User\Scripts\Logoff\" /y

reg add "HKCU\Software\Microsoft\Windows\CurrentVersion\Group Policy\Scripts\Logon\0" /v "GPO-ID" /t REG_SZ /d "LocalGPO" /f
reg add "HKCU\Software\Microsoft\Windows\CurrentVersion\Group Policy\Scripts\Logon\0" /v "SOM-ID" /t REG_SZ /d "Local" /f
reg add "HKCU\Software\Microsoft\Windows\CurrentVersion\Group Policy\Scripts\Logon\0" /v "FileSysPath" /t REG_SZ /d "C:\Windows\System32\GroupPolicy\User" /f
reg add "HKCU\Software\Microsoft\Windows\CurrentVersion\Group Policy\Scripts\Logon\0" /v "DisplayName" /t REG_SZ /d "Strat�gie de groupe locale" /f
reg add "HKCU\Software\Microsoft\Windows\CurrentVersion\Group Policy\Scripts\Logon\0" /v "GPOName" /t REG_SZ /d "Strat�gie de groupe locale" /f
reg add "HKCU\Software\Microsoft\Windows\CurrentVersion\Group Policy\Scripts\Logon\0" /v "PSScriptOrder" /t REG_DWORD /d "1" /f
reg add "HKCU\Software\Microsoft\Windows\CurrentVersion\Group Policy\Scripts\Logoff\0\0" /v "Script" /t REG_SZ /d "ClearIECache.cmd" /f
reg add "HKCU\Software\Microsoft\Windows\CurrentVersion\Group Policy\Scripts\Logoff\0\0" /v "Parameters" /t REG_SZ /d "" /f
reg add "HKCU\Software\Microsoft\Windows\CurrentVersion\Group Policy\Scripts\Logoff\0\0" /v "IsPowershell" /t REG_DWORD /d "0" /f

::CALL %~dp0Pack\%HOSTArch%\wumt_%HOSTArch%.exe -update 

:quitter
exit 
