@ECHO OFF
setlocal EnableDelayedExpansion

ver | find "10.0.15063" > nul
if errorlevel 1 (
	echo Cette version de Windows 10 n'est pas la bonne^^!
	echo la version attendue est la version 10.0.15063
	pause
	exit
)

echo.
echo | set /p=V�rification des permissions... 
net session >nul 2>&1
if errorlevel 1 (
	echo Permission refus�e. Lancez ce script en mode administrateur.
	pause
	exit
) else (
	echo Permission OK.
	timeout /t 1 > nul
)

:: Version 1.25 sans interface utilisateur (avec GPO)

::=============================================================================================================================
ECHO Param�tres / Confidentialit� 
::=============================================================================================================================
:: Chapitre 1.5 de la documentation
::---------------------------------------------------------------
ECHO. Param�tres / Confidentialit� / G�n�rale (3.1.1)
::---------------------------------------------------------------
:: (1) : Laisser les applications utiliser mon identifiant de publicit�
:: Gpedit / Configuration ordinateur / Mod�les d�administration / Syst�me / Profil utilisateur / D�sactiver l�ID de publicit�: Activ�
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\AdvertisingInfo" /v "DisabledByGroupPolicy" /t REG_DWORD /d "1" /f                                                   

:: (2)�: Permettre aux sites web d'acc�der � ma liste de langues pour fournir du contenu local
REG ADD "HKCU\Control Panel\International\User Profile" /v "HttpAcceptLanguageOptOut" /t REG_DWORD /d 1 /f    

:: (3) : Autoriser Windows � suivre les lancements d�applications pour am�liorer le menu D�marrer et les r�sultats de recherche.
REG ADD "HKCU\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced" /v "Start_TrackProgs" /t REG_DWORD /d "0" /f

::---------------------------------------------------------------
ECHO. Param�tres / Confidentialit� / Localisation (3.1.2)
::---------------------------------------------------------------
:: Gpedit / Configuration ordinateur / Mod�les d�administration / Composants Windows / Confidentialit� de l�application / Permettre aux applications Windows d�acc�der � la localisation / Activ� / Forcer le refus (ANSII)
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy" /v "LetAppsAccessLocation" /t REG_DWORD /d "2" /f
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy" /v "LetAppsAccessLocation_UserInControlOfTheseApps" /t REG_MULTI_SZ /f
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy" /v "LetAppsAccessLocation_ForceAllowTheseApps" /t REG_MULTI_SZ /f
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy" /v "LetAppsAccessLocation_ForceDenyTheseApps" /t REG_MULTI_SZ /f

:: Gpedit / Configuration Ordinateur / Mod�les d�administration / Composants Windows / Emplacement et capteur / D�sactiver l'emplacement�: Activ�
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\LocationAndSensors" /v "DisableLocation" /t REG_DWORD /d "1" /f
                                   
::---------------------------------------------------------------
ECHO. Param�tres / Confidentialit� / Cam�ra (3.1.3)
::---------------------------------------------------------------
::GPO
:: Gpedit / Configuration ordinateur / Mod�les d�administration / Composants Windows / Confidentialit� de l�application / Permettre aux applications Windows d�acc�der � l�appareil photo / Activ� / Forcer le refus (ANSII)
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy" /v "LetAppsAccessCamera" /t REG_DWORD /d "2" /f
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy" /v "LetAppsAccessCamera_UserInControlOfTheseApps" /t REG_MULTI_SZ  /f
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy" /v "LetAppsAccessCamera_ForceAllowTheseApps" /t REG_MULTI_SZ /f
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy" /v "LetAppsAccessCamera_ForceDenyTheseApps" /t REG_MULTI_SZ /f

::---------------------------------------------------------------
ECHO. Param�tres / Confidentialit� / Microphone (3.1.4)
::---------------------------------------------------------------
::GPO
:: Gpedit / Configuration ordinateur / Mod�les d�administration / Composants Windows / Confidentialit� de l�application / Permettre aux applications Windows d�acc�der au micro / Activ� / Forcer le refus (ANSII)
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy" /v "LetAppsAccessMicrophone" /t REG_DWORD /d "2" /f
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy" /v "LetAppsAccessMicrophone_UserInControlOfTheseApps" /t REG_MULTI_SZ /f
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy" /v "LetAppsAccessMicrophone_ForceAllowTheseApps" /t REG_MULTI_SZ /f
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy" /v "LetAppsAccessMicrophone_ForceDenyTheseApps" /t REG_MULTI_SZ /f

::---------------------------------------------------------------
ECHO. Param�tres / Confidentialit� / Notification (3.1.5)
::---------------------------------------------------------------
::GPO
:: Gpedit / Configuration ordinateur / Mod�les d�administration / Composants Windows / Confidentialit� de l�application / Laisser les applications Windows acc�der aux notifications / Activ� / Forcer le refus (ANSII)
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy" /v "LetAppsAccessNotifications" /t REG_DWORD /d "2" /f
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy" /v "LetAppsAccessNotifications_UserInControlOfTheseApps" /t REG_MULTI_SZ /f
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy" /v "LetAppsAccessNotifications_ForceAllowTheseApps" /t REG_MULTI_SZ /f
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy" /v "LetAppsAccessNotifications_ForceDenyTheseApps" /t REG_MULTI_SZ /f

::------------------------------------------------------------------------------
ECHO. Param�tres / Confidentialit� / Voix, entr�e manuscrite et frappe (3.1.6)
::------------------------------------------------------------------------------
::D�sactiver�: Apprendre � me conna�tre
REG ADD "HKCU\SOFTWARE\Microsoft\Personalization\Settings" /v "AcceptedPrivacyPolicy" /t REG_DWORD /d "0" /f
REG ADD "HKCU\SOFTWARE\Microsoft\InputPersonalization" /v "RestrictImplicitInkCollection" /t REG_DWORD /d "1" /f
REG ADD "HKCU\SOFTWARE\Microsoft\InputPersonalization" /v "RestrictImplicitTextCollection" /t REG_DWORD /d "1" /f
REG ADD "HKCU\SOFTWARE\Microsoft\InputPersonalization\TrainedDataStore" /v "AcceptedPrivacyPolicy" /t REG_DWORD /d "0" /f
REG ADD "HKCU\SOFTWARE\Microsoft\InputPersonalization\TrainedDataStore" /v "HarvestContacts" /t REG_DWORD /d "0" /f
::GPO
:: Gpedit.msc / Configuration ordinateur / Mod�les d�administration / Composants Windows / Panneau de configuration / Options r�gionales et linguistiques / Personnalisation de l��criture manuscrite / D�sactiver l�apprentissage automatique�: Activ�
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\InputPersonalization" /v "RestrictImplicitTextCollection" /t REG_DWORD /d "1" /f
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\InputPersonalization" /v "RestrictImplicitInkCollection" /t REG_DWORD /d "1" /f

:: Gpedit.msc / Configuration Ordinateur / Mod�les d'administration / Composants Windows / Panneau de configuration / Options r�gionales et linguistiques / Autoriser la personnalisation de la saisie: D�sactiv� (ANSSI)
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\InputPersonalization" /v "AllowInputPersonnalization" /t REG_DWORD /d "0" /f

:: Gpedit / Configuration ordinateur / Mod�les d�administration / Composants Windows / Voix / Autoriser la mise � jour automatique des donn�es vocale�: D�sactiv�  (Version 1703)
REG ADD "HKLM\Software\Policies\Microsoft\Speech" /v "AllowSpeechModelUpdate" /t REG_DWORD /d "0" /f

::-----------------------------------------------------------------------
ECHO. Param�tres / Confidentialit� / Informations sur le compte (3.1.7)
::-----------------------------------------------------------------------
::GPO
:: Gpedit / Configuration ordinateur / Mod�les d�administration / Composants Windows / Confidentialit� de l�application / Permettre aux applications Windows d�acc�der aux informations de compte�: Activ�: Forcer le refus (ANSII)
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy" /v "LetAppsAccessAccountInfo" /t REG_DWORD /d "2" /f
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy" /v "LetAppsAccessAccountInfo_UserInControlOfTheseApps" /t REG_MULTI_SZ /f
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy" /v "LetAppsAccessAccountInfo_ForceAllowTheseApps" /t REG_MULTI_SZ /f
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy" /v "LetAppsAccessAccountInfo_ForceDenyTheseApps" /t REG_MULTI_SZ /f
::-----------------------------------------------------
ECHO. Param�tres / Confidentialit� / Contacts (3.1.8)
::-----------------------------------------------------
::GPO
:: Gpedit / Configuration ordinateur / Mod�les d�administration / Composants Windows / Confidentialit� de l�application / Permettre aux applications Windows d�acc�der aux contacts�: Activ�: Forcer le refus (ANSII)
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy" /v "LetAppsAccessContacts" /t REG_DWORD /d "2" /f
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy" /v "LetAppsAccessContacts_UserInControlOfTheseApps" /t REG_MULTI_SZ /f
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy" /v "LetAppsAccessContacts_ForceAllowTheseApps" /t REG_MULTI_SZ /f
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy" /v "LetAppsAccessContacts_ForceDenyTheseApps" /t REG_MULTI_SZ /f

::-------------------------------------------------------
ECHO. Param�tres / Confidentialit� / Calendrier (3.1.9)
::-------------------------------------------------------
::GPO
:: Gpedit / Configuration ordinateur / Mod�les d�administration / Composants Windows / Confidentialit� de l�application / Permettre aux applications Windows d�acc�der au calendrier�: Activ�: Forcer le refus (ANSII)
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy" /v "LetAppsAccessCalendar" /t REG_DWORD /d "2" /f
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy" /v "LetAppsAccessCalendar_UserInControlOfTheseApps" /t REG_MULTI_SZ /f
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy" /v "LetAppsAccessCalendar_ForceAllowTheseApps" /t REG_MULTI_SZ /f
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy" /v "LetAppsAccessCalendar_ForceDenyTheseApps" /t REG_MULTI_SZ /f

::-------------------------------------------------------------------
ECHO. Param�tres / Confidentialit� / Historique des appels (3.1.10)
::-------------------------------------------------------------------
::GPO
:: Gpedit / Configuration ordinateur / Mod�les d�administration / Composants Windows / Confidentialit� de l�application / Permettre aux applications Windows d�acc�der � l�historique des appels�: Activ�: Forcer le refus (ANSII)
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy" /v "LetAppsAccessCallHistory" /t REG_DWORD /d 2 /f
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy" /v "LetAppsAccessCallHistory_UserInControlOfTheseApps" /t REG_MULTI_SZ /f
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy" /v "LetAppsAccessCallHistory_ForceAllowTheseApps" /t REG_MULTI_SZ /f
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy" /v "LetAppsAccessCallHistory_ForceDenyTheseApps" /t REG_MULTI_SZ /f

::-------------------------------------------------------------------
ECHO. Param�tres / Confidentialit� / Courrier �lectronique (3.1.11)
::-------------------------------------------------------------------
::GPO
:: Gpedit / Configuration ordinateur / Mod�les d�administration / Composants Windows / Confidentialit� de l�application / Permettre aux applications Windows d�acc�der aux e-mails / Activ� / Forcer le refus (ANSII)
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy" /v "LetAppsAccessEmail" /t REG_DWORD /d "2" /f
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy" /v "LetAppsAccessEmail_UserInControlOfTheseApps" /t REG_MULTI_SZ /f
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy" /v "LetAppsAccessEmail_ForceAllowTheseApps" /t REG_MULTI_SZ /f
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy" /v "LetAppsAccessEmail_ForceDenyTheseApps" /t REG_MULTI_SZ /f

::-------------------------------------------------------------------
ECHO. Param�tres / Confidentialit� / T�ches (RS2) (3.1.12)
::-------------------------------------------------------------------
::GPO
:: Gpedit / Configuration ordinateur / Mod�les d�administration / Composants Windows / Confidentialit� de l�application / Permettre aux applications Windows d�acc�der aux t�ches / Activ� / Forcer le refus (ANSII)
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy" /v "LetAppsAccessTasks" /t REG_DWORD /d "2" /f
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy" /v "LetAppsAccessTasks_UserInControlOfTheseApps" /t REG_MULTI_SZ /f
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy" /v "LetAppsAccessTasks_ForceAllowTheseApps" /t REG_MULTI_SZ /f
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy" /v "LetAppsAccessTasks_ForceDenyTheseApps" /t REG_MULTI_SZ  /f

::---------------------------------------------------------
ECHO. Param�tres / Confidentialit� / Messagerie (3.1.13)
::---------------------------------------------------------
::GPO
:: Gpedit / Configuration ordinateur / Mod�les d�administration / Composants Windows / Confidentialit� de l�application / Permettre aux applications Windows d�acc�der � la messagerie / Activ� / Forcer le refus (ANSII)
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy" /v "LetAppsAccessMessaging" /t REG_DWORD /d "2" /f
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy" /v "LetAppsAccessMessaging_UserInControlOfTheseApps" /t REG_MULTI_SZ /f
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy" /v "LetAppsAccessMessaging_ForceAllowTheseApps" /t REG_MULTI_SZ /f
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy" /v "LetAppsAccessMessaging_ForceDenyTheseApps" /t REG_MULTI_SZ  /f

::-----------------------------------------------------
ECHO. Param�tres / Confidentialit� / Radio (3.1.14)
::-----------------------------------------------------
::GPO
:: Gpedit / Configuration ordinateur / Mod�les d�administration / Composants Windows / Confidentialit� de l�application / Permettre aux applications Windows d�acc�der aux options de contr�le�: Activ� - Forcer le refus (ANSII)
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy" /v "LetAppsAccessRadios" /t REG_DWORD /d "2" /f
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy" /v "LetAppsAccessRadios_UserInControlOfTheseApps" /t REG_MULTI_SZ /f
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy" /v "LetAppsAccessRadios_ForceAllowTheseApps" /t REG_MULTI_SZ /f
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy" /v "LetAppsAccessRadios_ForceDenyTheseApps" /t REG_MULTI_SZ /f

::--------------------------------------------------------------
ECHO. Param�tres / Confidentialit� / Autres appareils (3.1.15)
::--------------------------------------------------------------
::GPO
:: Gpedit / Configuration ordinateur / Mod�les d�administration / Composants Windows / Confidentialit� de l�application / Permettre aux applications Windows d��tre synchronis�es avec les appareils / Activ� / Forcer le refus (ANSII)
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy" /v "LetAppsSyncWithDevices" /t REG_DWORD /d "2" /f
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy" /v "LetAppsSyncWithDevices_UserInControlOfTheseApps" /t REG_MULTI_SZ /f
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy" /v "LetAppsSyncWithDevices_ForceAllowTheseApps" /t REG_MULTI_SZ /f
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy" /v "LetAppsSyncWithDevices_ForceDenyTheseApps" /t REG_MULTI_SZ  /f

::------------------------------------------------------------------------
ECHO. Param�tres / Confidentialit� / Commentaires et diagnostics (3.1.16)
::------------------------------------------------------------------------
::GPO
:: (1) : Gpedit�: Configuration de l�ordinateur / Mod�les d�administration / Composants Windows / Collecte des donn�es et versions d�valuation Preview / Ne pas afficher les notifications de commentaires�: Activ� (ANSII)
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\DataCollection" /v "DoNotShowFeedbackNotifications" /t REG_DWORD /d "1" /f

:: (2) : Gpedit�: Configuration ordinateur / Mod�les d�administration / Composants Windows / Collecte des donn�es et versions d��valuation preview / Autoriser la t�l�m�trie�: Activ�: 1 � basique (pour les versions Pro et Home)  0 � Pour les versions Entreprise et �ducation  (ANSII)
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\DataCollection" /v "AllowTelemetry" /t REG_DWORD /d "0" /f

:: (3) Gpedit�: Configuration utilisateur / Mod�les d�administration / Composants Windows / Contenu cloud / Ne pas utiliser les donn�es de diagnostic pour personnaliser l�exp�rience utilisateur�: Activ�
REG ADD "HKCU\SOFTWARE\Policies\Microsoft\Windows\CloudContent" /v "DisableTailoredExperiencesWithDiagnosticData" /t REG_DWORD /d "1" /f

::--------------------------------------------------------------------------
ECHO. Param�tres / Confidentialit� / Applications en arri�re-plan (3.1.17)
::--------------------------------------------------------------------------
::GPO (RS2)
:: Gpedit / Configuration ordinateur / Mod�les d�administration / Composants Windows / Confidentialit� de l�application / Permettre aux applications Windows de s�ex�cuter en arri�re plan / Activ� / Forcer le refus (ANSII)
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy" /v "LetAppsRunInBackground" /t REG_DWORD /d "2" /f
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy" /v "LetAppsRunInBackground_UserInControlOfTheseApps" /t REG_MULTI_SZ /f
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy" /v "LetAppsRunInBackground_ForceAllowTheseApps" /t REG_MULTI_SZ /f
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy" /v "LetAppsRunInBackground_ForceDenyTheseApps" /t REG_MULTI_SZ  /f

::-----------------------------------------------------------------
ECHO. Param�tres / Confidentialit� / App Diagnostics (RS2) (3.1.18)
::-----------------------------------------------------------------
::GPO
:: Gpedit / Configuration ordinateur / Mod�les d�administration / Composants Windows / Confidentialit� de l�application / Permettre aux applications Windows d�acc�der aux informations de diagnostics / Activ� / Forcer le refus (ANSII)
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy" /v "LetAppsGetDiagnosticInfo" /t REG_DWORD /d "2" /f
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy" /v "LetAppsGetDiagnosticInfo_UserInControlOfTheseApps" /t REG_MULTI_SZ /f
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy" /v "LetAppsGetDiagnosticInfo_ForceAllowTheseApps" /t REG_MULTI_SZ /f
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy" /v "LetAppsGetDiagnosticInfo_ForceDenyTheseApps" /t REG_MULTI_SZ  /f

::---------------------------------------------------------------
ECHO. Param�tres / Confidentialit� / AUTRES Cl�s (RS2)  (3.1.19)
::---------------------------------------------------------------
::Ces trois cl�s trouv�es dans le registre non pas pour l�instant de correspondance avec un param�tre de l�interface utilisateur. Elles sont Activ�es par d�faut. D�sactivation
REG ADD "HKCU\SOFTWARE\Microsoft\Windows\CurrentVersion\DeviceAccess\Global\{235B668D-B2AC-4864-B49C-ED1084F6C9D3}" /v "Value" /t REG_SZ /d "Deny" /f 
REG ADD "HKCU\SOFTWARE\Microsoft\Windows\CurrentVersion\DeviceAccess\Global\{8c501030-f8c2-40b2-8b3b-e6605788ff39}" /v "Value" /t REG_SZ /d "Deny" /f 
REG ADD "HKCU\SOFTWARE\Microsoft\Windows\CurrentVersion\DeviceAccess\Global\{9D9E0118-1807-4F2E-96E4-2CE57142E196}" /v "Value" /t REG_SZ /d "Deny" /f

:: Ces trois GPO non pas pour l�instant de correspondance avec un param�tre de l�interface utilisateur. D�sactivation
:: Gpedit / Configuration ordinateur / Mod�les d�administration / Composants Windows / Confidentialit� de l�application / Permettre aux applications Windows d�acc�der aux donn�es de mouvements
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy" /v "LetAppsAccessMotion" /t REG_DWORD /d "2" /f
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy" /v "LetAppsAccessMotion_UserInControlOfTheseApps" /t REG_MULTI_SZ /f
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy" /v "LetAppsAccessMotion_ForceAllowTheseApps" /t REG_MULTI_SZ /f
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy" /v "LetAppsAccessMotion_ForceDenyTheseApps" /t REG_MULTI_SZ  /f

:: Gpedit / Configuration ordinateur / Mod�les d�administration / Composants Windows / Confidentialit� de l�application / Permettre aux applications Windows d�effectuer des appels t�l�phoniques
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy" /v "LetAppsAccessPhone" /t REG_DWORD /d "2" /f
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy" /v "LetAppsAccessPhone_UserInControlOfTheseApps" /t REG_MULTI_SZ /f
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy" /v "LetAppsAccessPhone_ForceAllowTheseApps" /t REG_MULTI_SZ /f
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy" /v "LetAppsAccessPhone_ForceDenyTheseApps" /t REG_MULTI_SZ  /f

:: Gpedit / Configuration ordinateur / Mod�les d�administration / Composants Windows / Confidentialit� de l�application / Permettre aux applications Windows d�acc�der aux appareils approuv�s : Activ� > forcer le refus
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy" /v "LetAppsAccessTrustedDevices" /t REG_DWORD /d "2" /f
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy" /v "LetAppsAccessTrustedDevices_UserInControlOfTheseApps" /t REG_MULTI_SZ /f
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy" /v "LetAppsAccessTrustedDevices_ForceAllowTheseApps" /t REG_MULTI_SZ /f
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy" /v "LetAppsAccessTrustedDevices_ForceDenyTheseApps" /t REG_MULTI_SZ  /f


::----------------------------------------------------------
ECHO. Param�tres / Comptes / Synchroniser vos param�tres (3.2.0)
::----------------------------------------------------------
::Pour D�sactiver la synchronisation dans le cloud de la messagerie
REG ADD "HKCU\Software\Microsoft\Messaging" /v "CloudServiceSyncEnabled" /t REG_DWORD /d "0" /f

::GPO
:: Gpedit / Configuration ordinateur / Mod�les d�administration / Composants Windows / Synchroniser vos param�tres / Ne pas synchroniser / Activ�
REG ADD "HKLM\Software\Policies\Microsoft\Windows\SettingSync" /v "DisableSettingSync" /t REG_DWORD /d "2" /f
REG ADD "HKLM\Software\Policies\Microsoft\Windows\SettingSync" /v "DisableSettingSyncUserOverride" /t REG_DWORD /d "1" /f

::----------------------------------------------------------
ECHO. TEREDO (3.3.0)
::----------------------------------------------------------
:: Gpedit.msc / Configuration ordinateur / Mod�les d'administration / Composants Windows / R�seau / Param�tres TCPIP / Technologie de transition IPV6 / D�finir l'�tat Teredo / Activ� > �tat D�sactiv�
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\TCPIP\v6Transition" /v  "Teredo_State" /t REG_SZ /d "Disabled" /f

::----------------------------------------------------
ECHO. Param�tres / R�seau et internet / Wi-fi (3.4.0)
::----------------------------------------------------
:: 1 � Param�tres / R�seau et Internet / Wi-Fi / G�rer les param�tres Wi-Fi / Se connecter selon les suggestions fournies, aux point d'acc�s ouvert (D�sactiv�)  
FOR /F "tokens=2*" %%a IN ('reg query "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Authentication\LogonUI" /v "LastLoggedOnUserSID" 2^>nul') do (SET UID=%%b)
REG ADD "HKLM\SOFTWARE\Microsoft\WcmSvc\wifinetworkmanager\features\%UID%" /v "FeatureStates" /t REG_DWORD /d 316 /f

:: Interdire la connexion automatique aux hotspotx Wi-Fi
reg add "HKLM\SOFTWARE\Microsoft\PolicyManager\default\WiFi\AllowAutoConnectToWiFiSenseHotspots" /v "value" /t REG_DWORD /d 0 /f 

:: Gpedit / Configuration ordinateur / Mod�les d�administration / R�seaux / Service de r�seau local sans fil / Param�tre de r�seau local sans fil / Emp�cher Windows de se connecter automatiquement aux points d'acc�s ouverts sugg�r�s, aux r�seaux partag�s par les contacts et aux points d'acc�s offrant des services payants : D�sactiv�  
REG ADD "HKLM\SOFTWARE\Microsoft\WcmSvc\wifinetworkmanager\config" /v "AutoConnectAllowedOEM" /t REG_DWORD /d 0 /f

:: Ne pas partager mes r�seau avec mes contacts de Compte Microsoft
 REG ADD "HKLM\SOFTWARE\Microsoft\WcmSvc\wifinetworkmanager\features\%UID%\SocialNetworks\ABCH" /v "OptInStatus" /t REG_DWORD /d 0 /f
:: Ne pas partager mes r�seau avec mes contacts Skype
 REG ADD "HKLM\SOFTWARE\Microsoft\WcmSvc\wifinetworkmanager\features\%UID%\SocialNetworks\ABCH-SKYPE" /v "OptInStatus" /t REG_DWORD /d 0 /f
:: Ne pas partager mes r�seau avec mes contacts Facebook
 REG ADD "HKLM\SOFTWARE\Microsoft\WcmSvc\wifinetworkmanager\features\%UID%\SocialNetworks\FACEBOOK" /v "OptInStatus" /t REG_DWORD /d 0 /f

::-----------------------------------------------------------------
ECHO. Param�tres / Personnalisation / �cran de verrouillage (3.6.0) 
::-----------------------------------------------------------------
:: D�sactivez: Personnaliser l'�cran de verrouillage, � partir de Windows et Cortana, notamment avec des anecdotes et des astuces.
REG ADD "HKCU\software\Microsoft\Windows\CurrentVersion\ContentDeliveryManager" /v "RotatingLockScreenOverlayEnabled" /t REG_DWORD /d 0 /f
REG ADD "HKCU\Software\Microsoft\Windows\CurrentVersion\ContentDeliveryManager" /v "RotatingLockScreenEnabled" /t REG_DWORD /d 0 /f

:: Param�tres / Personnalisation / Accueil / Afficher occasionnellement les suggestions dans l'�cran d'accueil: D�sactiv�
REG ADD "HKCU\Software\Microsoft\Windows\CurrentVersion\ContentDeliveryManager" /v "SystemPaneSuggestionsEnabled" /t REG_DWORD /d 0 /f 

:: Param�tres / Syst�me / Notifications et actions / Afficher les notifications dans l��cran de verrouillage : D�sactiv�.
REG ADD "HKCU\Software\Microsoft\Windows\CurrentVersion\Notifications\Settings" /v "NOC_GLOBAL_SETTING_ALLOW_TOASTS_ABOVE_LOCK" /t REG_DWORD /d "0" /f 

:: Param�tres / Syst�me / Notifications et actions / Obtenir des conseils, astuces et suggestions lorsque vous utilisez Windows : D�sactiv�.
REG ADD "HKCU\Software\Microsoft\Windows\CurrentVersion\ContentDeliveryManager" /v "SoftLandingEnabled" /t REG_DWORD /d "0" /f

:: Param�tres / Syst�me / Notifications et actions / Me montrer l'exp�rience de bienvenue apr�s une mies � jour et �ventuellement lorsque je me connecte pour mettre en avant les nouveaut�s et suggestions : D�sactiv�
REG ADD "HKCU\Software\Microsoft\Windows\CurrentVersion\ContentDeliveryManager" /v "SubscribedContent-310093Enabled" /t REG_DWORD /d "0" /f

:: Explorateur de fichier / Affichage / Options / Onglet Affichage : D�sactiver 'Afficher les notifications du fournisseur de synchronisation'
REG ADD "HKCU\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced" /v "ShowSyncProviderNotifications" /t REG_DWORD /d "0" /f 

:: GPO
:: GPedit / Configuration utilisateur / Mod�les d�administration / Composants Windows / Contenu cloud / D�sactiver toutes les fonctionnalit�s Windows � la une : Activ�.
REG ADD "HKCU\Software\Policies\Microsoft\Windows\CloudContent" /v "DisableWindowsSpotlightFeatures" /t REG_DWORD /d "1" /f

:: Gpedit / Configuration utilisateur / Mod�les d�administration / Composants Windows / Contenu Cloud / Configurer Windows � la une sur l��cran de verrouillage�: D�sactiv�
REG ADD "HKCU\Software\Policies\Microsoft\Windows\CloudContent" /v "ConfigureWindowsSpotlight" /t REG_DWORD /d "2" /f
REG ADD "HKCU\Software\Policies\Microsoft\Windows\CloudContent" /v "IncludeEnterpriseSpotlight" /t REG_DWORD /d "0" /f

::------------------------------------------------------------
ECHO. Param�tres / p�riph�rique / Stylet et Windows Ink  (3.33) 
::------------------------------------------------------------
:: Afficher les suggestions d'application recommand�es: D�sactiv�
REG ADD "HKCU\Software\Microsoft\Windows\CurrentVersion\PenWorkspace" /v "PenWorkspaceAppSuggestionsEnabled" /t REG_DWORD /d "0" /f

::===========================================================================================================
ECHO. D�sactive certains param�tres de Windows Defender (3.5.0)
::===========================================================================================================
REM Ne D�sactive pas Windows defender seulement les param�tres de t�l�m�trie
:: Gpedit.msc / Configuration Ordinateur / Mod�les d�administration / Composants Windows / Antivirus Windows Defender / D�sactiver Windows Defender : Activ� 
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows Defender" /v "DisableAntispyware" /t REG_DWORD /d "1" /f 

:: Gpedit.msc / Configuration Ordinateur / Mod�les d�administration / Composants Windows / Antivirus Windows Defender / MAPS / Rejoindre Microsoft MAPS�: Activ� (D�sactiv�)
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows Defender\Spynet" /v "SpyNetReporting" /t REG_DWORD /d "0" /f 

:: Gpedit.msc / Configuration Ordinateur / Mod�les d�administration / Composants Windows / Antivirus Windows Defender / Protection en temps r�el / D�sactiver la protection en temps r�el�: Activ�
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows Defender\Real-Time Protection" /v "DisableRealTimeMonitoring" /t REG_DWORD /d "1" /f

:: Gpedit.msc / Configuration Ordinateur / Mod�les d�administration / Composants Windows /  Antivirus Windows Defender / MAPS / Envoyer des exemples de fichier lorsqu�une analyse� : Activ� (Ne jamais envoyer)  
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows Defender\Spynet" /v "SubmitSamplesConsent" /t REG_DWORD /d "2" /f 

:: Gpedit.msc / Configuration Ordinateur / Mod�les d'administration / Composants Windows /  Antivirus Windows Defender / MAPS / Configurer une valeur de remplacement de param�tre locale pour l'envoi de rapport � Microsoft MAPS : D�sactiv�  (ANSSI)
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows Defender\Spynet" /v "LocalSettingOverrideSpynetReporting" /t REG_DWORD /d "0" /f 

:: Ne pas envoyer les infections a Microsoft (Cette cl� n�existe pas)
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\MRT" /v "DontReportInfectionInformation" /t REG_DWORD /d "1" /f 

::===========================================================================================================
ECHO. D�sactive Windows Store (3.7.0)
::===========================================================================================================
:: gpedit.msc / Configuration ordinateur / Mod�les d�administration / Composants Windows / Windows Store / D�sactiver toutes les applications du Windows Store�: Activ� (RS1 Entreprise et �ducation)
REG ADD "HKLM\Software\Policies\Microsoft\WindowsStore" /v "DisableStoreApps" /t REG_DWORD /d "1" /f

:: gpedit.msc / Configuration ordinateur / Mod�les d�administration / Composants Windows / Windows Store / D�sactiver l�application du Windows Store�: Activ�
REG ADD "HKLM\Software\Policies\Microsoft\WindowsStore" /v "RemoveWindowsStore" /t REG_DWORD /d "1" /f       

REM gpedit.msc / Configuration ordinateur / Mod�les d'administration / Composants Windows / Windows Store / D�sactiver le t�l�chargement automatique et l'installation des mises � jour : Activ� 
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\WindowsStore" /v "AutoDownload" /t REG_DWORD /d "2" /f 

REM gpedit.msc / Configuration ordinateur / Mod�les d'administration / Syst�me / Gestion de la communication internet / Param�tre de la communication internet / D�sactiver l'acc�s au windows Store / Activ�
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\Explorer" /v "NoUseStoreOpenWith" /t REG_DWORD /d "1" /f 

:: gpedit.msc / Configuration ordinateur / Mod�les d'administration / Composants Windows / Windows Store / Afficher uniquement le magasin priv� dans l'application du Windows Store: Activ� (ANSSI)
REG ADD "HKLM\Software\Policies\Microsoft\WindowsStore" /v "RequirePrivateStoreOnly" /t REG_DWORD /d "1" /f  

REM gpedit.msc / Configuration ordinateur / Mod�les d�administration / Composants Windows / Contenu Cloud / D�sactiver les exp�riences consommateur de Microsoft / Activ�
REG ADD "HKLM\Software\Policies\Microsoft\Windows\CloudContent" /v "DisableWindowsConsumerFeatures" /t REG_DWORD /d "1" /f 

REM gpedit.msc / Configuration utilisateur / Mod�les d'administration / Menu D�marrer et barre des t�ches / Ne pas autoriser l'�pinglage de l'application Windows Store � la barre des t�ches / Activ� 
REG ADD "HKCU\Software\Policies\Microsoft\Windows\Explorer" /v "NoPinningStoreToTaskbar" /t REG_DWORD /d "1" /f
 
REM gpedit.msc / Configuration ordinateur / Mod�les d�administration / Composants Windows / Windows Store / D�sactiver la proposition d�effectuer une mise � jour vers la derni�re version de Windows�: Activ�.
REG ADD "HKLM\Software\Policies\Microsoft\Windows\WindowsStore" /v "DisableOsUpgrade" /t REG_DWORD /d "1" /f      
 
::===========================================================================================================
ECHO. D�sactive Cortana (3.8.0)
::===========================================================================================================
:: D�sactiver Cortana (La recherche est toujours Activ�e)
reg add "HKLM\SOFTWARE\Microsoft\PolicyManager\default\Experience\AllowCortana" /v "value" /t REG_DWORD /d 0 /f
reg add "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Search" /v "CortanaEnabled" /t REG_DWORD /d 0 /f
reg add "HKCU\SOFTWARE\Microsoft\Windows\CurrentVersion\Search" /v "CortanaEnabled" /t REG_DWORD /d 0 /f
reg add "HKCU\SOFTWARE\Microsoft\Windows\CurrentVersion\Search" /v "CanCortanaBeEnabled" /t REG_DWORD /d 0 /f 
:: D�sactive Historique de mon appareil
REG ADD "HKCU\SOFTWARE\Microsoft\Windows\CurrentVersion\Search" /v "DeviceHistoryEnabled" /t REG_DWORD /d "0" /f

:: D�sactive affichage historique 
REG ADD "HKCU\SOFTWARE\Microsoft\Windows\CurrentVersion\Search" /v "HistoryViewEnabled" /t REG_DWORD /d "0" /f

:: D�sactiver Utiliser Cortana m�me si mon appareil est verrouill�
REG ADD "HKCU\Software\Microsoft\Speech_OneCore\Preferences" /v "VoiceActivationEnableAboveLockscreen" /t REG_DWORD /d "0" /f 

:: D�sactiver la recherche en ligne avec Bing dans Cortana
:: Si vous chercher une application locale ou un param�tre dans en utilisant la recherche de Windows 10 sans taper le nom exact, Windows 10 cherchera une r�ponse � travers Bing par d�faut au lieu d�utiliser votre disque local.
REG ADD "HKCU\SOFTWARE\Microsoft\Windows\CurrentVersion\Search" /v "BingSearchEnabled" /t REG_DWORD /d "0" /f

:: t�l�chargement et mises � jour des mod�les de reconnaissance vocale et de synth�se vocale D�sactiv�s
REG ADD "HKLM\SOFTWARE\Microsoft\Speech_OneCore\Preferences" /v "ModelDownloadAllowed" /t REG_DWORD /d "0" /f

:: GPO
::D�sactiver�: Autoriser la recherche et autoriser Cortana � utiliser l�emplacement
::Gpedit.msc / Configuration Ordinateur / Mod�les d�administration / Composants Windows / Rechercher / Autoriser la recherche et autoriser Cortana � utiliser l�emplacement�: D�sactiv�
REG ADD "HKLM\Software\Policies\Microsoft\Windows\Windows Search" /v "AllowSearchToUseLocation" /t REG_DWORD /d "0" /f       

::Ne pas autoriser la recherche Web
::Gpedit.msc / Configuration Ordinateur / Mod�les d�administration / Composants Windows / Rechercher / Ne pas autoriser la recherche Web�: Activ�
REG ADD "HKLM\Software\Policies\Microsoft\Windows\Windows Search" /v "DisableWebSearch" /t REG_DWORD /d "1" /f

:: Ne pas effectuer des recherches sur le Web ou afficher des r�sultats Web dans Search
:: Gpedit.msc / Configuration Ordinateur / Mod�les d�administration / Composants Windows / Rechercher / Ne pas effectuer des recherches sur le Web ou afficher des r�sultats Web dans Search�: Activer
REG ADD "HKLM\Software\Policies\Microsoft\Windows\Windows Search" /v "ConnectedSearchUseWeb" /t REG_DWORD /d "0" /f 

:: Ne pas effectuer des recherches sur le Web ou afficher des r�sultats Web dans Search (ANSSI)
:: Gpedit.msc / Configuration Ordinateur / Mod�les d'administration / Composants Windows / Rechercher / Ne pas effectuer des recherches sur le Web ou afficher des r�sultats Web dans Search via des connexions linit�es: Activer
REG ADD "HKLM\Software\Policies\Microsoft\Windows\Windows Search" /v "ConnectedSearchUseWebOverMeteredConnections" /t REG_DWORD /d "0" /f 

:: D�sactiver Cortana (La recherche est toujours Activ�e)
:: Gpedit.msc / Configuration Ordinateur / Mod�les d'administration / Composants Windows / Rechercher / Autoriser Cortana : D�sactiv�
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\Windows Search" /v "AllowCortana" /t REG_DWORD /d "0" /f

:: D�finir quelles informations sont partag�es dans search
:: GPEDIT / Configuration Ordinateur / Mod�les d'administration / Composants Windows / Rechercher / D�finir quelles informations sont partag�es dans search / Activ� : Informations anonymes 
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\Windows Search" /v "ConnectedSearchPrivacy" /t REG_DWORD /d "3" /f

:: D�finir le param�tres SafeSearch pour Search 
:: GPEDIT / Configuration Ordinateur / Mod�les d'administration / Composants Windows / Rechercher / D�finir le param�tres SafeSearch pour Search / Activ� : D�sactiv�
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\Windows Search" /v "ConnectedSearchSafeSearch" /t REG_DWORD /d "3" /f
	
:: Ne pas autoriser l�indexation des de fichiers chiffr�s (ANSSI)
:: Gpedit.msc / Configuration Ordinateur / Mod�les d�administration / Composants Windows / Rechercher / Autoriser l�indexation des fichiers chiffr�s�: D�sactiv�
REG ADD "HKLM\Software\Policies\Microsoft\Windows\Windows Search\CurrentPolicies" /v "AllowIndexingEncryptedStoresOrItems" /t REG_DWORD /d "0" /f   
			
:: D�sactiver Cortana au-dessus de l'�cran de verrouillage (ANSSI)
:: GPEDIT / Configuration Ordinateur / Mod�les d'administration / Composants Windows / Rechercher / Autoriser Cortana au-dessus de l'�cran de verrouillage : D�sactiv�
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\Windows Search" /v "AllowCortanaAboveLock" /t REG_DWORD /d "0" /f	
			
::===========================================================================================================
ECHO. Param�trage de Windows Update
::===========================================================================================================

:: Param�tres / Mise � jour et S�curit� / Windows Update / Options avanc�es / D�sactiver�: Choisir le mode de distribution des mises � jour�: D�sactiv� (D�sactive Windows Update via peer-to-peer) 
REG ADD "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\DeliveryOptimization\Config" /v "DODownloadMode" /t REG_DWORD /d "0" /f 
REG ADD "HKCU\SOFTWARE\Microsoft\Windows\CurrentVersion\DeliveryOptimization" /v "SystemSettingsDownloadMode" /t REG_DWORD /d "0" /f 

:: D�sactiver l'acc�s � la page Programme Windows Insider
Reg.exe add "HKLM\SOFTWARE\Microsoft\WindowsSelfHost\UI\Visibility" /v "HideInsiderPage" /t REG_DWORD /d "1" /f

:: D�sactiver l�acc�s aux mises � jour des versions Insider Preview
REG ADD "HKLM\SOFTWARE\Microsoft\WindowsSelfHost\Applicability" /v "EnablePreviewBuilds" /t REG_DWORD /d "0" /f 
REG ADD "HKLM\SOFTWARE\Microsoft\WindowsSelfHost\Applicability" /v "ThresholdFlightsDisabled" /t REG_DWORD /d "1" /f
REG ADD "HKLM\SOFTWARE\Microsoft\WindowsSelfHost\Applicability" /v "Ring" /t REG_SZ /d "Disabled" /f 

:: Param�tres / Mise � jour et S�curit� / Windows Update / Options avanc�es / Activer�: Me communiquer les mises � jour d�autres produits Microsoft lorsque je mets � jour Windows (ex. Silverlight, MsOffice...)
REG ADD "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\WindowsUpdate\Services\7971f918-a847-4430-9279-4a52d1efe18d" /v "RegisteredWithAU" /t REG_DWORD /d "1" /f 
REG ADD "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\WindowsUpdate\Services" /v "DefaultService" /t REG_SZ /d "7971f918-a847-4430-9279-4a52d1efe18d" /f

:: GPO
:: GPEDIT / Configuration Ordinateur / Mod�les d'administration / R�seau / Optimisation de la distribution / Mode de t�l�chargement / D�sactiv�
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\DeliveryOptimization" /v "DODownloadMode" /t REG_DWORD /d "0" /f

:: GPEDIT / Configuration Ordinateur / Mod�les d'administration / R�seau / Activer les services r�seau pair � pair Microsoft / D�sactiver les services r�seau pair � pair Microsoft : Activ�  (d�faut 0)
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Peernet" /v "Disabled" /t REG_DWORD /d "1" /f 

::Le param�trage suivant vous informera que des mises � jour sont disponibles, et vous d�ciderez du moment de les installer. (Seulement avec Windows 10 Pro, Entreprise et �ducation). Si vous souhaitez avoir les mises � jour automatiquement, ne modifiez rien.
:: gpedit.msc / configuration ordinateur / Mod�les d�administration / Composants Windows / Windows update / Configuration du service Mises � jour automatique (0=Active la mise � jour automatique, 1=D�sactive)
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU" /v "NoAutoUpdate" /t REG_DWORD /d "0" /f

::gpedit.msc / Configuration Ordinateur / Mod�les d�administration / Composant Windows / Windows Update / Configuration du service de mise � jour automatique (2=Notification des t�l�chargements et des installations) (3=t�l�chargement automatique et notification des installations) (4=t�l�charger automatiquement les mises � jour et les installer en fonction de la planification) (5=autoriser l�administrateur local � choisir les param�tres)
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU" /v "AUOptions" /t REG_DWORD /d "4" /f

:: gpedit.msc / Configuration Ordinateur / Mod�les d�administration / Composant Windows / Windows Update / Configuration du service de mise � jour (0=Tous les jours, 1=dimanche, 2=Lundi, 3=mardi, 4=mercredi, 5=jeudi, 6=vendredi, 7=samedi)
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU" /v "ScheduledInstallDay" /t REG_DWORD /d "0" /f

:: gpedit.msc / Configuration Ordinateur / Mod�les d�administration / Composant Windows / Windows Update / Configuration du service de mise � jour (l'heure de d�clenchement ici a 12 heures)
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU" /v "ScheduledInstallTime" /t REG_DWORD /d "12" /f

:: gpedit.msc / Configuration Ordinateur / Mod�les d�administration / Composant Windows / Windows Update / Diff�rer les mises � jour Windows / Choisir quand recevoir les mises � jour de fonctionnalit�s�: Activ�   (passer en Current Branche for Business CBB)
:: REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate" /v "DeferFeatureUpdates" /t REG_DWORD /d "1" /f
:: REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate" /v "BranchReadinessLevel" /t REG_DWORD /d "32" /f
:: REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate" /v "DeferFeatureUpdatesPeriodInDays" /t REG_DWORD /d "0" /f
:: REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate" /v "DeferFeatureUpdatesPeriodInDays" /t REG_DWORD /d "0" /f
:: Gpedit.msc / configuration ordinateur / Mod�les d�administration / Composants Windows / Windows Update / Ne pas se connecter � des emplacements Internet Winbdows Update : Activ� 
:: REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate" /v "DoNotConnectToWindowsUpdateInternetLocations" /t REG_DWORD /d "1" /f
:: Gpedit.msc / configuration ordinateur / Mod�les d�administration / Composants Windows / Windows Update / Supprimer l'acc�s � l'utilisation de toutes les fonctionnalit�s Windows update : Activ�
:: REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate" /v "SetDisableUXWUAccess" /t REG_DWORD /d "1" /f

:: gpedit.msc / Configuration Ordinateur / Mod�les d'administration / Composant Windows / Collect de donn�es et versions d'�valuation Preview / D�sactiver les fonctionnalit�s ou param�tres de pr�-version: D�sactiv� (ANSSI)
:: gpedit.msc / Configuration Ordinateur / Mod�les d'administration / Composant Windows / Collect de donn�es et versions d'�valuation Preview / Basculer le contr�le utilisateur sur les builds insider : D�sactiv� (ANSSI)
REG ADD "HKLM\Software\Policies\Microsoft\Windows\PreviewBuilds" /v "AllowBuildPreview" /t REG_DWORD /d "0" /f 

::===========================================================================================================
ECHO. D�sactive param�tre du navigateur (�1.5.10)
::===========================================================================================================
:: ---------------
ECHO IE (3.17.0)
:: ----------------

:: D�sactiver la g�olocalisation dans Internet Explorer
:: Gpedit / configuration ordinateur / Mod�les d�administration / Composants Windows / Internet Explorer / D�sactiver la g�olocalisation du navigateur�: Activ�
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Internet Explorer\Geolocation" /v "PolicyDisableGeolocation" /t REG_DWORD /d "1" /f

:: Gpedit / configuration ordinateur / Mod�les d�administration / Composants Windows / Internet Explorer / Activer les sites sugg�r�s�: D�sactiv�
REG ADD "HKLM\Software\Policies\Microsoft\Internet Explorer\Suggested Sites" /v "Enabled" /t REG_DWORD /d "0" /f

:: Gpedit / configuration ordinateur / Mod�les d�administration / Composants Windows / Internet Explorer / Autoriser les services Microsoft � fournir des suggestions am�lior�es � mesure que l�utilisateur entre du texte dans la barre d�adresses�: D�sactiv�
REG ADD "HKLM\Software\Policies\Microsoft\Internet Explorer" /v "AllowServicePoweredQSA" /t REG_DWORD /d "0" /f

:: Gpedit / configuration ordinateur / Mod�les d�administration / Composants Windows / Internet Explorer /  D�sactiver la fonctionnalit� de saisie semi-automatique des adresses Web�: Activ�
REG ADD "HKLM\Software\Policies\Microsoft\Windows\CurrentVersion\Explorer\AutoComplete" /v "AutoSuggest" /t REG_SZ /d "no" /f

:: Gpedit / configuration ordinateur / Mod�les d�administration / Composants Windows / Internet Explorer / Panneau de configuration Internet / Onglet Avanc� / V�rifier automatiquement les mises � jour d�Internet Explorer
REG ADD "HKLM\Software\Policies\Microsoft\Internet Explorer\Infodelivery\Restrictions" /v "NoUpdateCheck" /t REG_DWORD /d "1" /f

:: Activer la protection contre le tracking dans Internet Explorer
:: Gpedit / configuration ordinateur / Mod�les d�administration / Composants Windows / Internet Explorer / Confidentialit� / D�sactiver la protection contre le tracking�: Activ�
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Internet Explorer\Safety\PrivacIE" /v "DisableTrackingProtection" /t REG_DWORD /d "1" /f 

:: D�sactiver�: Smartscreen dans IE
:: Gpedit / configuration ordinateur / Mod�les d�administration / Composants Windows / Internet Explorer / Emp�cher la gestion du filtre SmartScreen�: Activ� > D�sactiv�
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Internet Explorer\PhishingFilter" /v "EnabledV9" /t REG_DWORD /d "0" /f 

:: Gpedit / configuration utilisateur / Mod�les d�administration / Composants Windows / Internet Explorer / Param�tres Internet / Param�tres avanc�s / Navigation / Masque le bouton (� c�t� du nouvel onglet) qui ouvre Microsoft Edge.
REG ADD "HKCU\SOFTWARE\Policies\Microsoft\Internet Explorer\main"  /v "HideNewEdgeButton" /t REG_DWORD /d "1" /f 

:: ---------------
ECHO Edge (3.18.0)
:: ----------------
:: Activer la protection contre le tracking dans Edge
REG ADD "HKCU\SOFTWARE\Classes\Local Settings\Software\Microsoft\Windows\CurrentVersion\AppContainer\Storage\microsoft.microsoftedge_8wekyb3d8bbwe\MicrosoftEdge\Main" /v "DoNotTrack" /t REG_DWORD /d "1" /f

:: D�sactiver�: Smartscreen dans Edge (Utilisateur)
REG ADD "HKCU\Software\Classes\Local Settings\Software\Microsoft\Windows\CurrentVersion\AppContainer\Storage\microsoft.microsoftedge_8wekyb3d8bbwe\MicrosoftEdge\PhishingFilter" /v "EnabledV9" /t REG_DWORD /d "0" /f

:: D�sactiver l'ouverture des sites avec les applications dans Edge (RS2)
REG ADD "HKCU\Software\Classes\Local Settings\Software\Microsoft\Windows\CurrentVersion\AppContainer\Storage\microsoft.microsoftedge_8wek yb3d8bbwe\MicrosoftEdge\AppLinks" /v "Enable"  /t REG_DWORD /d "0" /f

:: D�sactive la page de pr�diction dans Edge. Cette fonction semble envoyer les donn�es que vous tapez � Microsoft. Ceci �tait destin� � am�liorer la vitesse de votre navigateur pour l�affichage des pages.
REG ADD "HKCU\SOFTWARE\Classes\Local Settings\Software\Microsoft\Windows\CurrentVersion\AppContainer\Storage\microsoft.microsoftedge_8wekyb3d8bbwe\MicrosoftEdge\FlipAhead" /v "FPEnabled" /t REG_DWORD /d "0" /f
  
:: Ne pas laisser les sites Web enregistrer les licences des m�dias prot�g�es (DRM) (Utilisateur courant)
REG ADD "HKCU\Software\Classes\Local Settings\Software\Microsoft\Windows\CurrentVersion\AppContainer\Storage\microsoft.microsoftedge_8wekyb3d8bbwe\MicrosoftEdge\Privacy" /v "EnableEncryptedMediaExtensions" /t REG_DWORD /d "0" /f   

:: GPO
:: D�sactiver Configurer la saisie automatique
:: Gpedit.msc / Configuration ordinateur / Mod�les d�administration / Composants Windows / Microsoft Edge / Configurer la saisie automatique�: D�sactiv� 
REG ADD "HKLM\Software\Policies\Microsoft\MicrosoftEdge\Main" /v "Use FormSuggest" /t REG_SZ /d "no" /f

:: Gpedit.msc / configuration ordinateur / Mod�les d�administration / Composants Windows / Microsoft Edge / Configurer Ne pas suivre�: D�sactiv� 
REG ADD "HKLM\Software\Policies\Microsoft\MicrosoftEdge\Main" /v "DoNotTrack" /t REG_DWORD /d "0" /f

:: D�sactiver�: Smartscreen dans Edge (2 GPO pour la m�me cl� de registre)
:: Gpedit.msc / configuration ordinateur / Mod�les d'administration / Composants Windows / Windows Defender Smartscreen / Microsoft Edge / Configurer Windows Defender Smartscreen: D�sactiv�
:: Gpedit.msc / Configuration ordinateur / Mod�les d�administration / Composants Windows / Microsoft Edge / Configurer Windows Defender SmartScreen�: D�sactiv�
REG ADD "HKLM\Software\Policies\Microsoft\MicrosoftEdge\PhishingFilter" /v "EnabledV9" /t REG_DWORD /d "0" /f

:: D�sactiver�: Configurer les suggestions dans la barre d�adresse.
:: Gpedit.msc / configuration ordinateur / Mod�les d�administration / Composants Windows / Microsoft Edge / Configurer les suggestions dans la barre d�adresse�: D�sactiv�
REG ADD "HKLM\Software\Policies\Microsoft\MicrosoftEdge\SearchScopes" /v "ShowSearchSuggestionsGlobal" /t REG_DWORD /d "0" /f

:: Envoyer tout le trafic Intranet vers internet Explorer
:: Gpedit.msc / configuration ordinateur / Mod�les d�administration / Composants Windows / Microsoft Edge / Envoyer tout le trafic Intranet vers internet Explorer
REG ADD "HKLM\Software\Policies\Microsoft\MicrosoftEdge\Main" /v "SendIntranetTraffictoInternetExplorer" /t REG_DWORD /d "1" /f

::===========================================================================================================
ECHO. D�sactive Param�tres de Onedrive (3.14)
::===========================================================================================================

:: Fermeture du process OneDrive
TASKKILL /F /IM OneDrive.exe /T
:: gpedit.msc / Configuration ordinateur / Mod�les d�administration / Composants Windows / OneDrive / Emp�cher l�utilisation de OneDrive pour le stockage de fichiers�: Activ� (ANSSI)
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\OneDrive" /v "DisableFileSyncNGSC" /t REG_DWORD  /d "1" /f
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\OneDrive" /v "DisableLibrariesDefaultSaveToOneDrive" /t REG_DWORD /d "1" /f
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\OneDrive" /v "DisableMeteredNetworkFileSync" /t REG_DWORD /d "1" /f
:: Retrait Onedrive du Panneau lat�ral de l�explorateur. 
REG ADD "HKCR\CLSID\{018D5C66-4533-4307-9B53-224DE2ED1FE6}" /v "System.IsPinnedToNameSpaceTree" /t REG_DWORD /d "0" /REG:32 /f
REG ADD "HKCR\CLSID\{018D5C66-4533-4307-9B53-224DE2ED1FE6}" /v "system.IsPinnedToNameSpaceTree" /t REG_DWORD /d "0" /REG:64 /f
REG ADD "HKCU\SOFTWARE\CLASSES\CLSID\{018D5C66-4533-4307-9B53-224DE2ED1FE6}" /v "System.IsPinnedToNameSpaceTree" /t REG_DWORD /d "0" /REG:32 /f
REG ADD "HKCU\SOFTWARE\CLASSES\CLSID\{018D5C66-4533-4307-9B53-224DE2ED1FE6}" /v "system.IsPinnedToNameSpaceTree" /t REG_DWORD /d "0" /REG:64 /f
:: Retrait Onedrive du d�marrage automatique lors de l�ouverture de session.
REG DELETE "HKCU\SOFTWARE\Microsoft\Windows\CurrentVersion\Run" /v "OneDrive" /f

sc config OneSyncSvc start= disabled
net stop OneSyncSvc > NUL 2>&1
reg add "HKLM\System\CurrentControlSet\Services\OneSyncSvc" /v "Start" /t REG_DWORD /d 4 /f > NUL 2>&1

::===========================================================================================================
ECHO. D�sactive d'autres param�tres de t�l�m�trie
::===========================================================================================================

::--------------------------------------------------------------------
ECHO. 3.9 -  Rapport d'erreurs
:: D�sactivation des rapports d'erreurs
:: Gpedit.msc / Configuration Ordinateur / Mod�le d'administration / Composants Windows / Rapport d'erreurs Windows / D�sactiver Rapport d'erreurs Windows : Activ�
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\Windows Error Reporting" /v "Disabled" /t REG_DWORD /d "1" /f
:: Gpedit.msc / Configuration Ordinateur / Mod�le d'administration / Composants Windows / Rapport d�erreurs Windows / Envoyer automatiquement des images m�moire pour les rapports d�erreurs g�n�r�s par le syst�me d�exploitation�: D�sactiv� (ANSSI)
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\Windows Error Reporting" /v "AutoApproveOSDumps" /t REG_DWORD /d "0" /f
:: Gpedit.msc / Configuration Ordinateur / Mod�le d'administration / Composants Windows / Rapport d'erreurs Windows / Na pas envoyer de donn�es compl�mentaires : Activ� (ANSSI)
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\Windows Error Reporting" /v "DontSendAdditionalData" /t REG_DWORD /d "1" /f
::Gpedit.msc / Configuration Ordinateur / Mod�le d�administration / Syst�me / Gestion de la communication Internet / Param�tres de communication Internet / D�sactiver Rapport d�erreurs Windows�: Activ� (ANSSI)
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\PCHealth\ErrorReporting" /v "DoReport" /t REG_DWORD /d "0" /f
:: Autres
REG ADD "HKLM\SOFTWARE\Microsoft\Windows NT\CurrentVersion\UnattendSettings\Windows Error Reporting" /v "DisableWER" /t REG_DWORD /d "1" /f 
REG ADD "HKLM\SOFTWARE\Microsoft\Windows\Windows Error Reporting\WMR" /v "Disable" /t REG_DWORD /d "1" /f
REG ADD "HKLM\SOFTWARE\Microsoft\Windows\Windows Error Reporting\Consent" /v "DefaultConsent" /t REG_DWORD /d "0" /f


::-------------------------------------------------------
ECHO. 3.11 Pas de v�rification de licence
:: Par d�faut, Windows v�rifie votre licence chaque fois que vous allumez votre PC, cela emp�che le rep�rage.
:: gpedit.msc / Configuration Ordinateur / Mod�les d�administration / Composant Windows / Plateforme de protection de licence logicielle / D�sactiver la validation AVS en ligne de client KMS / Activ�
REG ADD "HKLM\Software\Policies\Microsoft\Windows NT\CurrentVersion\Software Protection Platform" /v "NoGenTicket" /t REG_DWORD /d "1" /f

::-------------------------------------------------------
ECHO. 3.12 Bloquer le Service "Auto IP-Listening" (recueil d'adresse IP � chaque connexion). 
:: ATTENTION�! ce param�tre peut marquer l�indicateur de connexion internet comme inactif alors que la connexion est bonne.
REG ADD "HKLM\SYSTEM\CurrentControlSet\Services\NlaSvc\Parameters\Internet" /v "EnableActiveProibing" /t REG_DWORD /d "1" /f
:: Gpedit.msc / Configuration Ordinateur / Mod�les d�administration / Syst�me / Gestion de la communication Internet / Param�tres de Communication Interne / D�sactiver les tests actifs de l�indicateur de statut de connectivit� r�seau Windows�: Activ�
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\NetworkConnectivityStatusIndicator" /v "NoActiveProbe" /t REG_DWORD /d "1" /f  

::-------------------------------------------------------
ECHO. 3.13 D�sactivation des conseils Windows
:: gpedit.msc / Configuration ordinateur / Mod�les d�administration / Composants Windows / Contenu Cloud / Ne pas afficher les conseils de Windows�: Activ�
REG ADD "HKLM\Software\Policies\Microsoft\Windows\CloudContent" /v "DisableSoftLanding" /t REG_DWORD /d "1" /f 
:: gpedit.msc / Configuration ordinateur / Mod�les d�administration / Composants Windows / Contenu Cloud / D�sactiver les exp�riences consommateur de Microsoft�: Activ�
REG ADD "HKLM\Software\Policies\Microsoft\Windows\CloudContent" /v "DisableWindowsConsumerFeatures" /t REG_DWORD /d "1" /f

::-----------------------------------------------------
ECHO. 3.15 D�sactiver la t�l�m�trie 
:: (1 sur 3)
:: Gpedit.msc / Configuration Ordinateur / Mod�les d�administration / Composants Windows / Collecte des donn�es et versions d��valuation Pr�view / Autoriser la t�l�m�trie�: Activ� - 0
:: Param�tres / Confidentialit� / Commentaires & Diagnostics / Envoyer les donn�es de l�appareil � Microsoft / de base gris�e 
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\DataCollection" /v "AllowTelemetry" /t REG_DWORD /d "0" /f 
:: D�sactiver la t�l�m�trie (2 sur 3) 
REG ADD "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\DataCollection" /v "AllowTelemetry" /t REG_DWORD /d 0 /f  
:: D�sactiver la t�l�m�trie applicative (3 sur 3)
:: Gpedit.msc / configuration ordinateur / Mod�les d�administration / Composants Windows / Compatibilit� des applications / D�sactiver la t�l�m�trie applicative�: Activ�
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\AppCompat" /v "AITEnable" /t REG_DWORD /d "0" /f                                          

:: Gpedit.msc / Configuration Ordinateur / Mod�les d'administration / Composants Windows / Collecte des donn�es et versions d'�valuation Pr�view / D�sactiver les fonctionnalit�s ou param�tres de pr�-version : D�sactiv� (ANSSI)
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\DataCollection" /v "EnableConfigFlighting" /t REG_DWORD /d "0" /f 


::-----------------------------------------------------
ECHO. 3.16 D�sactiver des vignettes dynamiques
:: Gpedit.msc / Configuration utilisateur / Mod�les d�administration / Menu D�marrer et barre des t�ches / Notifications / D�sactiver l�utilisation du r�seau pour les notifications�: Activ�
REG ADD "HKCU\SOFTWARE\Policies\Microsoft\Windows\CurrentVersion\PushNotifications" /v "NoCloudApplicationNotification" /t REG_DWORD /d "1" /f

:: Gpedit.msc / Configuration utilisateur / Mod�les d'administration / Menu D�marrer et barre des t�ches / Notifications / D�sactiver les notifications par vignette : Activ�
REG ADD "HKCU\SOFTWARE\Policies\Microsoft\Windows\CurrentVersion\PushNotifications" /v "NoTileApplicationNotification" /t REG_DWORD /d "1" /f

::-----------------------------------------------------
ECHO. 3.19 Vie priv�e
:: D�sactiver le partage des donn�es de personnalisation de l��criture manuscrite
:: Gpedit.msc / Configuration Ordinateur / Mod�les d�administration / Syst�me / Gestion de la communication internet / Param�tre de communication internet / D�sactiver le partage des donn�es de personnalisation de l��criture manuscrite�: Activ�
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\TabletPC" /v "PreventHandwritingDataSharing" /t REG_DWORD /d "1" /f

:: D�sactiver le signalement d�erreurs de la reconnaissance de l��criture manuscrite
:: Gpedit.msc / Configuration Ordinateur / Mod�les d'administration / Syst�me / Gestion de la communication internet / Param�tre de communication internet / D�sactiver le signalement d�erreurs de la reconnaissance de l��criture manuscrite�: Activ�
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\HandwritingErrorReports" /v "PreventHandwritingErrorReports" /t REG_DWORD /d "1" /f 

:: D�sactiver l'Inventory Collector
:: L�Inventory Collector r�pertorie les applications, fichiers, p�riph�riques et pilotes pr�sents dans le syst�me et envoie ces informations � Microsoft. Cela permet ensuite de diagnostiquer les probl�mes de compatibilit�.
:: Gpedit.msc / Configuration Ordinateur / Mod�les d�administration / Composants Windows / Compatibilit� des applications / D�sactiver l'Inventory Collector�: Activ�
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\AppCompat" /v "DisableInventory" /t REG_DWORD /d "1" /f 

:: Emp�cher l�activation de la cam�ra de l��cran de verrouillage (pas de reconnaissance faciale)
:: Gpedit.msc / Configuration Ordinateur / Mod�les d�administration / Panneau de configuration / Personnalisation / Emp�cher l�activation de l�appareil photo de l��cran de verrouillage�: Activ�
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\Personalization" /v "NoLockScreenCamera" /t REG_DWORD /d "1" /f

:: D�sactiver les notifications d�application
:: Les applications ne pourront plus afficher de notifications sur les tuiles, l��cran ou le bureau
REG ADD "HKCU\SOFTWARE\Microsoft\Windows\CurrentVersion\PushNotifications" /v "ToastEnabled" /t REG_DWORD /d "0" /f

:: D�sactive les publicit� venant par le Bluetooth.
REG ADD "HKLM\SOFTWARE\Microsoft\PolicyManager\current\device\Bluetooth" /v "AllowAdvertising" /t REG_DWORD /d "0" /f

:: D�sactive l'Identifiant de publicit� et le remet � zero
REG DELETE "HKCU\SOFTWARE\Microsoft\Windows\CurrentVersion\AdvertisingInfo" /v "ID" /f
REG ADD "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\AdvertisingInfo" /v "Enabled" /t REG_DWORD /d "0" /f

:: Ce r�glage emp�che durant les phases d�exp�rimentation de windows, d'�tudier les pr�f�rences de l'utilisateur ou le comportement de ses devices.
REG ADD "HKLM\SOFTWARE\Microsoft\PolicyManager\current\device\System" /v "AllowExperimentation" /t REG_DWORD /d "0" /f

:: Ne pas faire partie du programme Windows Customer Experience Improvement Program (CEIP) (defaut 1)
REG ADD "HKLM\SOFTWARE\Microsoft\Windows NT\CurrentVersion\UnattendSettings\SQMClient" /v "CEIPEnabled" /t REG_DWORD /d "0" /f

:: D�sactiver le Programme d�am�lioration de l�exp�rience utilisateur Windows
:: gpedit.msc / Configuration ordinateur / Mod�les d�administration / Syst�me / Gestion de la communication Internet / Param�tres de communication Internet / D�sactiver le Programme d�am�lioration de l�exp�rience utilisateur Windows�: Activ�
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\SQMClient\Windows" /v "CEIPEnable" /t REG_DWORD /d "0" /f

::----------------------------------------------------
ECHO. 3.20 G�olocalisation et service de positionnement
:: En compl�ment de la rubrique Param�tres / Confidentialit� / Localisation
:: D�sactiver l�emplacement (1 sur 2)
:: Gpedit.msc / Configuration Ordinateur / Mod�les d�administration / Composants Windows / Emplacement et capteur / D�sactiver l�emplacement�: Activ�
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\LocationAndSensors" /v "DisableLocation" /t REG_DWORD /d "1" /f

:: D�sactiver le service de localisation Windows (2 sur 2)
:: Gpedit.msc / Configuration Ordinateur / Mod�les d�administration / Composants Windows / Emplacement et capteur / Service de localisation Windows / D�sactiver le service de localisation Windows�: Activ�
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\LocationAndSensors" /v "DisableWindowsLocationProvider" /t REG_DWORD /d "1" /f

:: D�sactiver le script d�emplacement
:: Gpedit.msc / Configuration Ordinateur / Mod�les d�administration / Composants Windows / Emplacement et capteur / D�sactiver le script d�emplacement�: Activ�
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\LocationAndSensors" /v "DisableLocationScripting" /t REG_DWORD /d "1" /f 

:: D�sactiver les capteurs
:: Gpedit.msc / Configuration Ordinateur / Mod�les d�administration / Composants Windows / Emplacement et capteur / D�sactiver les capteurs�: Activ�
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\LocationAndSensors" /v "DisableSensors" /t REG_DWORD /d "1" /f 

:: D�sactiver le service de G�olocalisation 
REG ADD "HKLM\SYSTEM\ControlSet001\Services\lfsvc" /v "Start" /t REG_DWORD /d 4 /f
REG ADD "HKLM\SYSTEM\ControlSet001\Services\lfsvc\Service\Configuration" /v "Status" /t REG_DWORD /d "0" /f

::--------------------------------------------------------------------
ECHO 3.21 - Compte Microsoft
:: gpedit / Configuration Ordinateur / Param�tres Windows / Param�tres de S�curit� / Strat�gie Locales / Option de S�curit� / Comptes : Bloquer les Comptes Microsoft : Les utilisateurs ne peuvent pas ajouter de comptes Microsoft, ni se connecter avec ces derniers (ANSSI)
REG ADD "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System" /v "NoConnectedUser"  /t REG_DWORD /d 3 /f

::--------------------------------------------------------------------
ECHO 3.22 - Emp�cher la r�cup�ration des m�tadonn�es de p�riph�rique depuis internet
:: Panneau de configuration / Syst�me / Param�tres syst�me avanc�s / Onglet Mat�riel / Param�tres d'installation des p�riph�riques / Activ� (votre appareil peut ne pas fonctionner comme attendu) (d�faut = 0)
REG ADD "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Device Metadata" /v "PreventDeviceMetadataFromNetwork" /t REG_DWORD /d "1" /f 
::GPO
:: gpedit.msc / Configuration Ordinateur / Mod�les d'administration / Syst�me / Installation de p�riph�rique / Emp�cher la r�cup�ration des m�tadonn�es de p�riph�rique depuis internet / Activ�
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\Device Metadata" /v "PreventDeviceMetadataFromNetwork" /t REG_DWORD /d "1" /f

::--------------------------------------------------------------------
ECHO. 3.23 -  Bloquer les applications sugg�r�es
REG ADD "HKCU\software\Microsoft\Windows\CurrentVersion\ContentDeliveryManager" /v "SilentInstalledAppsEnabled" /t REG_DWORD /d "0" /f 

::---------------------------------------------------------------------
ECHO. 3.24 - Localiser mon appareil
:: GPEDIT / Configuration de l�ordinateur / Mod�le d�administration / Composants Windows / Localiser mon appareil / Activer/D�sactiver l�option Localiser mon appareil�: D�sactiv�
REG ADD "HKLM\software\Policies\Microsoft\FindMyDevice" /v "AllowFindMyDevice" /t REG_DWORD /d "0" /f 

::---------------------------------------------------------------------
ECHO 3.25 - Param�tres / Applications / Cartes hors connexion
:: Vous pouvez D�sactiver la possibilit� de t�l�charger et de mettre � jour les cartes hors connexion.
:: gpedit / Configuration ordinateur / Mod�les d�administration / Composants Windows / Cartes / D�sactiver le t�l�chargement automatique et la mise � jour des donn�es cartographiques.
REG ADD "HKLM\software\Policies\Microsoft\Windows\Maps" /v "AutoDownloadAndUpdateMapData" /t REG_DWORD /d "0" /f 

:: Dans Windows�10, version 1607 et ult�rieures, appliquez la strat�gie de groupe�:
:: gpedit / Configuration ordinateur / Mod�les d�administration / Composants Windows / Cartes / D�sactiver le trafic r�seau non sollicit� sur la page Param�tres des cartes hors connexion.
REG ADD "HKLM\software\Policies\Microsoft\Windows\Maps" /v "AllowUntriggeredNetworkTrafficOnSettingsPage" /t REG_DWORD /d "0" /f 

 
::--------------------------------------------------------------------
ECHO. 3.26 -  Divers
:: GPEDIT / Configuration Ordinateur / Mod�les d�administration / Syst�me / Gestion de la communication internet / Param�tres de communication internet / D�sactiver les mises � jour des fichiers de contenu de l�Assistant recherche
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\SearchCompanion" /v "DisableContentFileUpdates" /t REG_DWORD /d "1" /f	

:: GPEDIT / Configuration Ordinateur / Mod�les d'administration / Syst�me / D�pannage et diagnostics / Windows Performane Perftrack / Activer/D�sactiver Perftrack : D�sactiv�
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\WDI\{9c5a40da-b965-4fc3-8781-88dd50a6299d}" /v "ScenarioExecutionEnabled" /t REG_DWORD /d "0" /f

:: GPEDIT / Configuration Ordinateur / Mod�les d'administration / Syst�me / Installation de p�riph�rique / Ne pas envoyer de rapport d'erreurs Windows lors de l'installation d'un pilote g�n�rique sur un p�riph�rique : Activ�
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\DeviceInstall\Settings" /v "DisableSendGenericDriverNotFoundToWER" /t REG_DWORD /d "1" /f


:: Cl� prot�g�e (Utilisation de SETACL)
::Powershell -command (get-acl -path "hklm:\SYSTEM\DriverDatabase").sddl
::Powershell -command "get-acl -path "hklm:\SYSTEM\DriverDatabase" | Format-List"
Powershell -command "(get-acl -path "hklm:\SYSTEM\ControlSet001\Services\DPS").sddl" > SauveACL.txt
FOR /F %%i in ('type SauveACL.txt') do echo "machine\SYSTEM\DriverDatabase",4,"%%i" > SauveACL.txt
Powershell -command(setACL -on "HKLM\SYSTEM\DriverDatabase" -ot reg -actn setowner -ownr "n:S-1-5-32-544;s:y" -rec yes >nul 2>&1 )
Powershell -command(setACL -on "HKLM\SYSTEM\DriverDatabase" -ot reg -actn ace -ace "n:S-1-5-32-544;s:y;p:full" -rec yes >nul 2>&1 )
REG ADD "HKLM\SYSTEM\DriverDatabase\Policies\Settings" /v "DisableSendGenericDriverNotFoundToWER" /t REG_DWORD /d "1" /f
Powershell -command(SetACL -on "HKLM\SYSTEM\DriverDatabase" -ot reg -actn restore -bckp SauveACL.txt >nul 2>&1)
DEL SauveACL.txt >nul 2>&1 

:: D�sactive Help Experience Improvement Program (HEIP) collect en envoie � MS la fa�on dont vous utilisez l'aide 
REG ADD "HKCU\SOFTWARE\Microsoft\Assistance\Client\1.0\Settings" /v "ImplicitFeedback" /t REG_DWORD /d "0" /f

:: Arr�ter Smartscreen de Windows
:: Param�tres / Mise � jour et s�curit� / Windows Defender / Ouvrir le centre de s�curit� Windows Defender / Contr�le des applications et du navigateur / V�rifier les applications et les fichiers / D�sactiv� 
REG ADD "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer" /v "SmartScreenEnabled" /t REG_SZ /d "Off" /f

:: Gpedit.msc / configuration ordinateur / Mod�les d'administration / Composants Windows / Windows Defender Smartscreen / Explorateur / Configurer Windows Defender Smartscreen: D�sactiv�
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\System" /v "EnableSmartScreen" /t REG_DWORD /d "0" /f

:: Gpedit.msc / configuration ordinateur / Mod�les d�administration / Composants Windows / Flux RSS / Emp�cher la d�couverte automatique des flux et des composants Web Slice
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Internet Explorer\Feed Discovery" /v "Enabled" /t REG_DWORD /d "0" /f  

:: Gpedit.msc / configuration ordinateur / Mod�les d�administration / Composants Windows / Synchroniser vos param�tres / Ne pas synchroniser les param�tres d'application : Activ� 
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\SettingSync" /v "DisableApplicationSettingSync" /t REG_DWORD /d "2" /f
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\SettingSync" /v "DisableApplicationSettingSyncUserOverride" /t REG_DWORD /d "0" /f

:: Gpedit.msc / configuration ordinateur / Mod�les d�administration / Composants Windows / Windows Mail / Turn Off Windows Mail application : Activ�
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows Mail" /v "ManualLaunchAllowed" /t REG_DWORD /d "0" /f

:: D�sactiver les conseils de la barre de jeu
REG ADD "HKCU\Software\Microsoft\GameBar" /v "ShowStartupPanel" /t REG_DWORD /d "0" /f

:: Gpedit.msc / Configuration ordinateur / Mod�les d'administration / Composants Windows / R�seau / Police / Activer les fournisseurs de polices / D�sactiv�
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\System" /v "EnableFontProviders" /t REG_DWORD /d "0" /f

:: Gpedit.msc / Configuration ordinateur / Mod�les d'administration / Syst�me / Strat�gie de groupe / Poursuivre les experiences sur cet appareil / D�sactiv�
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\System" /v "EnableCdp" /t REG_DWORD /d "0" /f

:: D�sactivez l'alerte "Vous avez de nouvelles applications qui peuvent ouvrir ce type de fichier"  
reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows\Explorer" /v "NoNewAppAlert" /t REG_DWORD /d 1 /f > nul

REM gpedit.msc / Configuration ordinateur / Mod�les d'administration / Syst�me / Gestion de la communication internet / Param�tre de la communication internet / D�sactiver la mise � jour automatique des certificats racine : Activ�
REM Attention sur certain site https ne seront pas disponible
REM REG ADD "HKLM\SOFTWARE\Policies\Microsoft\SystemCertificates\AuthRoot" /v "DisableRootAutoUpdate" /t REG_DWORD /d "1" /f

::-------------------------------------------------------------------
ECHO 3.28 - D�sactiver Windows Insider
::D�sactiver l�acc�s aux mises � jour des versions Insider Preview
REG ADD "HKLM\SOFTWARE\Microsoft\WindowsSelfHost\Applicability" /v "EnablePreviewBuilds" /t REG_DWORD /d "0" /f 
REG ADD "HKLM\SOFTWARE\Microsoft\WindowsSelfHost\Applicability" /v "ThresholdFlightsDisabled" /t REG_DWORD /d "1" /f
REG ADD "HKLM\SOFTWARE\Microsoft\WindowsSelfHost\Applicability" /v "Ring" /t REG_SZ /d "Disabled" /f 

:: D�sactiver l�acc�s � la page Programme Windows Insider
REG ADD "HKLM\SOFTWARE\Microsoft\WindowsSelfHost\UI\Visibility" /v "HideInsiderPage" /t REG_DWORD /d "0" /f

:: Gpedit.msc / Configuration Ordinateur / Mod�les d�administration / Composant Windows / Collecte de donn�es et versions d��valuation Preview / D�sactiver les fonctionnalit�s ou param�tres de pr�-version�: D�sactiv�
REG ADD "HKLM\Software\Policies\Microsoft\Windows\PreviewBuilds" /v "AllowBuildPreview" /t REG_DWORD /d "0" /f 

::--------------------------------------------------------------------
ECHO 4.3.9 Media player
:: D�sactivation de l'envoi de donn�es � MS via Media player
REG ADD "HKCU\Software\Microsoft\MediaPlayer\Preferences" /v "UsageTracking" /t REG_DWORD /d "0" /f

:: Ne pas Envoyer un Identificateur d'Utilisateur pour M�dia Player
REG ADD "HKCU\Software\Microsoft\MediaPlayer\Preferences" /v "SendUserGUID" /t  REG_BINARY /d "00" /f

:: Ne pas t�l�charger des informations sur les m�dias 
REG ADD "HKCU\Software\Microsoft\MediaPlayer\Preferences" /v "MetadataRetrieval" /t REG_DWORD /d "0" /f

:: D�sactiver le t�l�chargement des licences pour les fichiers multim�dias 
REG ADD "HKCU\Software\Microsoft\MediaPlayer\Preferences" /v "SilentAcquisition" /t REG_DWORD /d "0" /f

:: D�sactiver la v�rification automatique des licences de fichiers prot�g�s
REG ADD "HKCU\Software\Microsoft\MediaPlayer\Preferences" /v "DisableLicenseRefresh" /t REG_DWORD /d "1" /f

::REG ADD "HKCU\Software\Microsoft\MediaPlayer\Preferences" /v "SilentAcquisition" REG_BINARY /d "00"
::REG ADD "HKCU\Software\Microsoft\MediaPlayer\Preferences" /v "UpgradeCodecPrompt" /t REG_DWORD /d "0" /f
::REG ADD "HKCU\Software\Microsoft\MediaPlayer\Preferences" /v "DisableMRU" REG_BINARY /d "01"
::REG ADD "HKCU\Software\Microsoft\MediaPlayer\Preferences" /v "AutoAddMusicToLibrary" /t REG_DWORD /d "0" /f
::REG ADD "HKCU\Software\Microsoft\MediaPlayer\Preferences" /v "StartInMediaGuide" /t REG_DWORD /d "0" /f 



::--------------------------------------------------------------------
::ECHO 3.1.32
:: D�sactiver le contenu ��Le saviez-vous�?�� du centre d�aide et support
:: gpedit.msc / Configuration ordinateur / Mod�les d�administration / Syst�me / Gestion de la communication Internet / Param�tres de communication Internet / D�sactiver le contenu ��Le saviez-vous�?�� du centre d�aide et support.
::REG ADD "HKLM\SOFTWARE\Policies\Microsoft\PCHealth\HelpSvc" /v "HeadLines" /t REG_DWORD /d "0" /f

::===========================================================================================================
:: ECHO. D�sactivation de Windows M�dia Player
::===========================================================================================================
:: DISM /online /Disable-Feature /FeatureName:WindowsMediaPlayer



::===========================================================================================================
ECHO. Nettoyage du dossier AutoLogger
::===========================================================================================================
:et
IF NOT EXIST "%SYSTEMDRIVE%\ProgramData\Microsoft\Diagnosis\ETLLogs\AutoLogger\AutoLogger-Diagtrack-Listener.etl" ( GOTO :CHECK2 ) >nul
IF EXIST "%SYSTEMDRIVE%\ProgramData\Microsoft\Diagnosis\" ( 2>nul TAKEOWN /F "%SYSTEMDRIVE%\ProgramData\Microsoft\Diagnosis" /A /r /d o >nul & 2>nul ICACLS "%SYSTEMDRIVE%\ProgramData\Microsoft\Diagnosis" /grant:r *S-1-5-32-544:F /T /C >nul ) >nul
IF EXIST "%SYSTEMDRIVE%\ProgramData\Microsoft\Diagnosis\ETLLogs\AutoLogger\AutoLogger-Diagtrack-Listener.etl" ( TAKEOWN /F "%SYSTEMDRIVE%\ProgramData\Microsoft\Diagnosis\ETLLogs\AutoLogger\AutoLogger-Diagtrack-Listener.etl" >nul & ICACLS "%SYSTEMDRIVE%\ProgramData\Microsoft\Diagnosis\ETLLogs\AutoLogger\AutoLogger-Diagtrack-Listener.etl" /reset /T /Q >nul & DEL /F /Q "%SYSTEMDRIVE%\ProgramData\Microsoft\Diagnosis\ETLLogs\AutoLogger\AutoLogger-Diagtrack-Listener.etl" >nul ) >nul
:CHECK2
IF NOT EXIST "%SYSTEMDRIVE%\ProgramData\Microsoft\Diagnosis\ETLLogs\ShutdownLogger\AutoLogger-Diagtrack-Listener.etl" ( GOTO :CHECK3 ) >nul
IF EXIST "%SYSTEMDRIVE%\ProgramData\Microsoft\Diagnosis\ETLLogs\ShutdownLogger\AutoLogger-Diagtrack-Listener.etl" ( TAKEOWN /F "%SYSTEMDRIVE%\ProgramData\Microsoft\Diagnosis\ETLLogs\ShutdownLogger\AutoLogger-Diagtrack-Listener.etl" >nul & ICACLS "%SYSTEMDRIVE%\ProgramData\Microsoft\Diagnosis\ETLLogs\ShutdownLogger\AutoLogger-Diagtrack-Listener.etl" /reset /T /Q >nul & DEL /F /Q "%SYSTEMDRIVE%\ProgramData\Microsoft\Diagnosis\ETLLogs\ShutdownLogger\AutoLogger-Diagtrack-Listener.etl" >nul ) >nul
:CHECK3
IF NOT EXIST "%SYSTEMDRIVE%\ProgramData\Microsoft\Diagnosis\" ( GOTO :flushdns ) >nul
IF EXIST "%SYSTEMDRIVE%\ProgramData\Microsoft\Diagnosis\" ICACLS "%SYSTEMDRIVE%\ProgramData\Microsoft\Diagnosis" /remove:g SYSTEM /inheritance:r /deny SYSTEM:(OI)(CI)F >nul


::--------------------------------------------------
ECHO Edge
::----------------------------------------------------
:: D�sactivation de la completion automatique de l'adresse Web dans la barre d'adresse
REG ADD "HKLM\SOFTWARE\Microsoft\PolicyManager\current\device\Browser" /v "AllowAddressBarDropdown" /t REG_DWORD /d "0" /f

:: Gpedit.msc / configuration ordinateur / Mod�les d�administration / Composants Windows / Microsoft Edge / Configurer le gestionnaire de mot de passe : D�sactiv�
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\MicrosoftEdge\Main" /v "FormSuggest Passwords" /t REG_SZ /d "no" /f

:: Gpedit.msc / configuration ordinateur / Mod�les d'administration / Composants Windows / Microsoft Edge / Autoriser le contenu web dans un nouvel onglet : D�sactiv�
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\MicrosoftEdge\ServiceUI" /v "AllowWebContentOnNewTabPage" /t REG_DWORD /d "0" /f 

:: D�sactive PSR
:: reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows\AppCompat" /v "DisableUAR" /t REG_DWORD /d 1 /f

:: w10 1709
:: REG ADD "HKCU\Software\Policies\Microsoft\InputPersonalization" /v "AllowInputPersonalization" /t REG_DWORD /d "0" /f 

pause