#Author : Florian BOBIN @ruskof_
#
#
#
param (
    [Parameter(Mandatory=$True,Position=1)]
        [string]$TemplateName, #Name of the template to use
    [Parameter(Mandatory=$True,Position=2)] 
        [string]$NewVMName, #Name of the new virtual machine to create (based on template)
        [string]$vSwitch #Name of the switch to use on the new VM
)

#######################
#      VARIABLES      #
#######################

#Templates path
[string]$TemplatePath = "D:\Hyper-V\Templates";

#VM storage path
[string]$NewVMPath = "D:\Hyper-V\Virtual Hard Disks"

#Log file path
[string]$LogPath = "D:\Hyper-V\deploy_template.log";

#Default switch to use if nothing is specified in args
if(!$vSwitch)
{
    [string]$vSwitch = "Default Switch" 
}

######################
#        EXEC        #
######################

#Log file "init"
if($(Test-Path $LogPath))
{
    Clear-Content $LogPath;
}
Add-Content -Path $LogPath -Value "Script exec at $(Get-Date)"; 
Add-Content -Path $LogPath -Value "***************************************************************************************************";
Add-Content -Path $LogPath -Value "vSwitch used : $vSwitch -  $(Get-Date)";

#Check if VHDX file exists
$VHDCheck = Test-Path "$TemplatePath\$TemplateName.vhdx";
if(!$VHDCheck)
{
    Write-Host "VHD not found :( " -ForegroundColor "Red";
    Add-Content -Path $LogPath -Value "Checking VHD if VHD exists - VHD not found :( - FAILED - $(Get-Date)";
    Exit;
}

#Copy VHDX file to regular storage path
try 
{   
    Add-Content -Path $LogPath -Value "Start copying VHD - $(Get-Date)";

    #VHDX file copy
    Start-BitsTransfer -Source "$TemplatePath\$TemplateName.vhdx" -Destination "$NewVMPath\$NewVMName.vhdx" -DisplayName "Copying template VHDx file..."

    Add-Content -Path $LogPath -Value "Finish copying VHD - $(Get-Date)";
    Add-Content -Path $LogPath -Value "Copying the VHD disk - SUCCESS - $(Get-Date)";
}
catch 
{
    Write-Host "Error :( - Check logs !" -ForegroundColor "Red";
    Add-Content -Path $LogPath -Value "Copying the VHD disk - FAILED - $(Get-Date)";
    Add-Content -Path $LogPath -Value $_.Exception.Message;
    Exit;
}

#VM creation with basic settings
try 
{
    New-VM -Name $NewVMName `
            -Generation 2 `
            -MemoryStartupBytes 1GB `
            -SwitchName $vSwitch `
            -VHDPath "$NewVMPath\$NewVMName.vhdx" `
            -BootDevice VHD | Out-Null
    
    Set-VM -Name $NewVMName `
            -DynamicMemory `
            -MemoryMinimumBytes 1GB `
            -MemoryMaximumBytes 4GB `
            -AutomaticStartAction Nothing `
            -AutomaticStopAction ShutDown | Out-Null

    Add-Content -Path $LogPath -Value "Virtual Machine creation - SUCCESS - $(Get-Date)";
}
catch 
{
    Write-Host "Error :( - Check logs !" -ForegroundColor "Red";
    Add-Content -Path $LogPath -Value "Virtual Machine creation - FAILED - $(Get-Date)";
    Add-Content -Path $LogPath -Value $_.Exception.Message;
    Exit;              
}